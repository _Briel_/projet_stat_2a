---
title: "Saint-Petersbourg"
output:
  pdf_document: default
  html_document: default
date: "2023-03-20"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Statistique descriptives sur la ville de Saint-Petersbourg

Ce document a pour but d'offrir un certain nombre de statistiques descriptives utiles à l'étude du lien entre le nombre de contestation contre la politique de démolition-reconstruction à Saint-Petersbourg.
Il ne s'agit pas d'un document destiné à être publié, mais à être utile à l'étude.
Ici les statistiques proposées concernent la ville de Saint-Petersbourg et peuvent notamment servir à comparer les carréctéristiques des quartiers de rénovation par rapport au reste de la ville.

Nous commencons par charger la table des bâtiments de la ville.

```{r}
library(dplyr)
library(readr)
BUILDINGS <- read.csv("~/work/projet_stat_2a/code_commun/stat_descriptive/BUILDINGS_base.csv")

nb_protest <- unique(BUILDINGS$nb_protest)
RZT <- unique(BUILDINGS$RZT)
district <- unique(BUILDINGS$district)
nb_resident <- sum(na.omit(BUILDINGS$nb_residents))
```
La ville compte 5018449 habitants (d'après les autorités) et comprend 23 quartiers de rénovation.

# Statistiques  de résumé basique

Ce résumé permet de voir le type des variables disponibles ainsi que certaines statistiques basiques.

```{r}
summary(BUILDINGS[,3:99])  #on ne regarde pas les stats sur les identifiants des bâtiments
```
# Statistiques descriptives sur la formes des appartements dans les bâtiments
Dans la ville il y a : 
- 21046 appartements communautaires de 2 pièces
- 36973 appartements communautaires de 3 pièces
- 16951 appartements communautaires de 4 pièces
- 7209 appartements communautaires de 5 pièces
- 3486 appartements communautaires de 6 pièces
- 8816 appartements communautaires de 7 pièces ou plus
- 552710 appartements individuels de 1 pièces
- 773802 appartements individuels de 2 pièces
- 531806 appartements individuels de 3 pièces
- 72537 appartements individuels de 4 pièces
- 7867 appartements individuels de 5 pièces
- 2019 appartements individuels de 6 pièces
- 15602 appartements individuels de 7 pièces ou plus


```{r}
nb_comm_flat_2 <- sum(BUILDINGS$X2_room_comm)
nb_comm_flat_3 <- sum(BUILDINGS$X3_room_comm)
nb_comm_flat_4 <- sum(BUILDINGS$X4_room_comm)
nb_comm_flat_5 <- sum(BUILDINGS$X5_room_comm)
nb_comm_flat_6 <- sum(BUILDINGS$X6_room_comm)
nb_comm_flat_7 <- sum(BUILDINGS$X7_room_comm)

nb_indiv_flat_1 <- sum(BUILDINGS$X1_room_flat)
nb_indiv_flat_2 <- sum(BUILDINGS$X2_room_flat)
nb_indiv_flat_3 <- sum(BUILDINGS$X3_room_flat)
nb_indiv_flat_4 <- sum(BUILDINGS$X4_room_flat)
nb_indiv_flat_5 <- sum(BUILDINGS$X5_room_flat)
nb_indiv_flat_6 <- sum(BUILDINGS$X6_room_flat)
nb_indiv_flat_7 <- sum(BUILDINGS$X7_room_flat)

```

La ville compte 2050824 appartements répartis dans 22228 bâtiments. 
Il y a 4,61% d'appartements communautaires (soit 94481 appartements) et 95.39% d'appartements individuels (soit 1956343 appartements).

```{r}
nb_total_com_flats <- sum(BUILDINGS$nb_com_flats)
nb_total_indiv_flats <- sum(BUILDINGS$nb_flats)
nb_total_all_flats <- sum(BUILDINGS$nb_all_flats)
pourcentage_com_flats <- (nb_total_com_flats/nb_total_all_flats)*100
pourcentage_indiv_flats <- (nb_total_indiv_flats/nb_total_all_flats)*100
```
Comme les appartements communautaires correspondent par définition à plusieurs ménages, il est intéressant de compléter ces observations à l'échelle des appartements par une observation à l'échelle des pièces.
La ville compte 4439553 pièces. 
Il y a 8,05% des pièces qui sont dans des appartements communautaires (soit 357443 pièces) et 91.95% des pièces qui sont dans des appartements individuels (soit 4082110 pièces).

```{r}
nb_total_com_rooms <- sum(BUILDINGS$nb_com_rooms)
nb_total_indiv_rooms <- sum(BUILDINGS$nb_rooms)
nb_total_all_rooms <- sum(BUILDINGS$nb_all_rooms)
pourcentage_com_rooms <- (nb_total_com_rooms/nb_total_all_rooms)*100
pourcentage_indiv_rooms <- (nb_total_indiv_rooms/nb_total_all_rooms)*100
```

La ville compte 5018449 résidents. Il y a en moyenne 0.41 appartements par résident (appartements individuels et communautaires confondus) et en moyenne 0.88 pièces par résident (pièces individuelles et communautaires confondues). Chaque résidents dispose en moyenne de 23.49 m² habitables.

```{r}
nb_total_living_area <- sum(na.omit(BUILDINGS$living_area))
nb_moy_flats_resid <- nb_total_all_flats/nb_resident
nb_moy_rooms_resid <- nb_total_all_rooms/nb_resident
nb_moy_living_area_resid <-  nb_total_living_area/nb_resident
```

# Statistique descriptives sur le type et la gestion des batiments

La table suivant résume les styles architecturaux des bâtiments dans la ville :

```{r}
table(BUILDINGS$style_archi)
```
La table suivant résume la gestion des bâtiments dans la ville :


```{r}
table(BUILDINGS$management)
```

Dans la ville il y a 18.51% des bâtiment gérés de façon coopérative et 80.18% gérés par des entreprises (la gestion du reste des bâtiments n'est pas renseignée).

```{r}
nb_total_buildings <- nrow(BUILDINGS)
nb_management_syndic <- sum(na.omit(BUILDINGS$management=="syndic"))
nb_management_coop <- sum(na.omit(BUILDINGS$management=="coop"))
nb_management_company <- sum(na.omit(BUILDINGS$management=="company"))
pourcentage_syndic_coop <- ((nb_management_syndic + nb_management_coop)/nb_total_buildings)*100
pourcentage_company <- (nb_management_company/nb_total_buildings)*100
```

Dans la ville, 13422 bâtiments sur 22228 ont subi des réparations majeures, ce qui correspond à 60.38% des bâtiments.
Il y a 3535 bâtiments qui ont été reconstruits, soit 15.90% des bâtiments. 

```{r}
nb_repair <- nb_total_buildings-sum(is.na(BUILDINGS$date_repair))
pourcentage_repair <- (nb_repair/nb_total_buildings)*100
nb_reconstruction <- nb_total_buildings-sum(is.na(BUILDINGS$date_reconstruction))
pourcentage_reconstruction <- (nb_reconstruction/nb_total_buildings)*100
```

Dans la ville il y 201 bâtiments délabrés, soit 0.90% des bâtiments.

```{r}
nb_dilapidated <- sum(na.omit(BUILDINGS$dilapidated)=="oui")
pourcentage_dilapidated <- (nb_dilapidated/nb_total_buildings)*100
```

Dans la ville il y 8724 bâtiments avec ascenseur, soit 39.25% des bâtiments.
Dans la vile il y 6072 bâtiments avec vide ordures , soit 27.32% des bâtiments.
Il y a 21058 bâtiments dont le chauffage est central, 549 bâtiments dont le chauffage est assuré par des chaudières et 584 bâtiments dont le chauffage est assuré par des poêles. Ce la correspond respectivement à 94.74%, 2.47% et 2.63% des bâtiments.


```{r}
nb_total_lift <- length(BUILDINGS$lift_nb[which(!BUILDINGS$lift_nb==0)])
pourcentage_lift <- (nb_total_lift/nb_total_buildings)*100
nb_total_waste_chute <- sum(BUILDINGS$waste_chute=="oui", na.rm = TRUE)
pourcentage_waste_chute <- (nb_total_waste_chute/nb_total_buildings)*100
nb_total_heating_central <- sum(BUILDINGS$heating_central=="oui")
nb_total_heating_boiler <- sum(BUILDINGS$heating_boiler=="oui")
nb_total_heating_stove <- sum(BUILDINGS$heating_stove=="oui")
pourcentage_heating_central <- (nb_total_heating_central/nb_total_buildings)*100
pourcentage_heating_boiler <- (nb_total_heating_boiler/nb_total_buildings)*100
pourcentage_heating_stove <- (nb_total_heating_stove/nb_total_buildings)*100
```

Dans la ville il y a 14594 bâtiments dont le chauffage de l'eau est central, 6760 bâtiments dont le chauffage de l'eau est assuré au gaz et 25 bâtiments dont le chauffage de l'eau est assuré au bois. Ce la correspond respectivement à 65.66%, 30.41% et 0.11% des bâtiments.

```{r}
nb_total_water_central <- sum(BUILDINGS$water_central=="oui")
nb_total_water_gas <- sum(BUILDINGS$water_gas=="oui")
nb_total_water_wood <- sum(BUILDINGS$water_wood=="oui")
pourcentage_water_central <- (nb_total_water_central/nb_total_buildings)*100
pourcentage_water_gas <- (nb_total_water_gas/nb_total_buildings)*100
pourcentage_water_wood <- (nb_total_water_wood/nb_total_buildings)*100
```

Dans la ville il y a 22167 bâtiments dont l'énergie pour l'éclairage et la cuisine est de l'électricité centrale, 17631 bâtiments dont cette énergie est assuré au gaz central et 639 bâtiments dont cette énergie est assuré au gaz individuel. Ce la correspond respectivement à 99.73%, 79.31% et 2.87% des bâtiments (certains bâtiments cumulent plusieurs systèmes).

```{r}
nb_total_electr_central <- sum(BUILDINGS$electr_central=="oui")
nb_total_gas_central <- sum(BUILDINGS$gas_central=="oui")
nb_total_gas_noncentral <- sum(BUILDINGS$gas_noncentral=="oui")
pourcentage_electr_central <- (nb_total_electr_central/nb_total_buildings)*100
pourcentage_gas_central <- (nb_total_gas_central/nb_total_buildings)*100
pourcentage_gas_non_central <- (nb_total_gas_noncentral/nb_total_buildings)*100
```

