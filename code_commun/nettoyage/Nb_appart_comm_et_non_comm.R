buildings <- read.csv("projet_stat_2a/Sources/buildings.csv")

library(dplyr)
library(readr)


# Nombre d'appartements selon qu'ils soient communautaire ou non ----------

# nb_com_flats : nb d'appart comm
# nb_flats : nb d'appart non comm

nb_com_and_non_com_flats <- buildings %>% 
  summarise(nb_com_flats = sum(nb_com_flats, na.rm = T),
            nb_non_com_flats = sum(nb_flats, na.rm = T)
            )

# Il y a 94481 appart comm
# Il y a 1956343 appart non comm


# Nombre d'appartements selon qu'ils soient communautaire ou non par tye --

chemin <- "projet_stat_2a/steve/tables_jointure"
fichiers_long <- list.files(path = chemin, pattern = "appartment_type_wide.csv$")

for (fichier in fichiers_long){
  assign(str_remove_all(fichier, "\\.csv$"),
         read_csv(paste(chemin, fichier, sep = "/"))[,-c(1)])
}

nb_com_by_type <- colSums(comm_appartment_type_wide[,-c(1)], na.rm = T)
nb_com_flat_by_type <- colSums(flat_appartment_type_wide[,-c(1)], na.rm = T)

plot_nb_com_by_type <- barplot(
  nb_com_by_type,
  names.arg = c("2 rooms", "3 rooms", "4 rooms", "5 rooms", "6 rooms", "7 rooms")
  )

plot_nb_com_flat_by_type <- barplot(
  nb_com_flat_by_type,
  names.arg = c("1 room", "2 rooms", "3 rooms", "4 rooms", "5 rooms", "6 rooms", "7 rooms")
)

# --- . ---
