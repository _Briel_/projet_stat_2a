#############################################################################################################################
#MISE EN FORME
#############################################################################################################################
#import des packages
#install.packages("Hmisc")
library(dplyr)
library(Hmisc)
library(stringr)
library(stringi)
library(tidyr)
library(readr)

###    import des données    ###
buildings <- read.csv("~/work/projet_stat_2a/Sources/buildings.csv")
str(buildings)

### pré traitement de la table ###
#transformation des num 1/0 en facteur oui/non
buildings$dilapidated <-factor(x = buildings$dilapidated, levels=c(1, 0), labels=c("oui", "non"))
buildings$heating_central <-factor(x = buildings$heating_central, levels=c(1, 0), labels=c("oui", "non"))
buildings$heating_boiler <-factor(x = buildings$heating_boiler, levels=c(1, 0), labels=c("oui", "non"))
buildings$heating_stove <-factor(x = buildings$heating_stove, levels=c(1, 0), labels=c("oui", "non"))
buildings$water_central <-factor(x = buildings$water_central, levels=c(1, 0), labels=c("oui", "non"))
buildings$water_gas <-factor(x = buildings$water_gas, levels=c(1, 0), labels=c("oui", "non"))
buildings$water_wood <-factor(x = buildings$water_wood, levels=c(1, 0), labels=c("oui", "non"))
buildings$electr_central <-factor(x = buildings$electr_central, levels=c(1, 0), labels=c("oui", "non"))
buildings$gas_central <-factor(x = buildings$gas_central, levels=c(1, 0), labels=c("oui", "non"))
buildings$gas_noncentral <-factor(x = buildings$gas_noncentral, levels=c(1, 0), labels=c("oui", "non"))
buildings$waste_chute <-factor(x = buildings$waste_chute, levels=c(1, 0), labels=c("oui", "non"))

# Traitement à la main pré-traitmt code (quelques caractères ou coquilles ont été retirés)

buildings$date_repair[4085] <- "2006"
buildings$date_repair[8609] <- "2002; 2006; 2006; 2007; 2008; 2008; 2011; 2012; 2013; 2013"
buildings$date_repair[11608] <- "2002; 2005; 2005; 2006; 2007; 2007; 2009" 
buildings$date_repair[13296] <- "2007; 2007; 2010; 2010; 2010; 2010"

# Création de colonnes sur reconstruction/repair date ---------------------

buildings <- buildings %>% 
  mutate(
    unrecognisable_date_reconstruction = !str_detect(date_reconstruction, pattern = "(^\\d{4}$)|^$"),
    first_date_repair = str_extract(date_repair, pattern = "^\\d{4}"),
    last_date_repair = str_extract(date_repair, pattern = "\\d{4}$"),
    nb_repair = str_count(date_repair, pattern = "\\d{4}"),
    unrecognisable_date_repair = !str_detect(date_repair, pattern = "(^(\\d{4} *(,|;|/|-)* *(\\d{4})* *)+$)|^$"),
  )

max_date_string <- function(vecteur){
  max(unlist(str_extract_all(vecteur, "\\d{4}")))
}

mes_vars <- unlist(lapply(buildings$date_reconstruction[buildings$unrecognisable_date_reconstruction], max_date_string))
buildings$date_reconstruction[buildings$unrecognisable_date_reconstruction] <- mes_vars

buildings$date_reconstruction[buildings$date_reconstruction == ""] <- NA

#buildings$date_reconstruction[buildings$unrecognisable_date_reconstruction] <- str_extract(buildings$date_reconstruction[buildings$unrecognisable_date_reconstruction], "\\d{4}")

buildings$unrecognisable_date_reconstruction <- NULL
buildings$unrecognisable_date_repair <- NULL

#transformation des "" en "NA"
buildings <- apply(buildings, 2, function(x) ifelse(x=="",NA,x))
buildings <- data.frame(buildings)

# Liste dates de construction pas belles (code de Steve)
library(stringr)
liste_dates_pas_bien <- unique(buildings$date_construction[str_detect(buildings$date_construction, "^(?!\\d{4}$).+")])
str(liste_dates_pas_bien, nchar.max = 125)

# Classification styles
library(dplyr)
buildings <- buildings %>%
  mutate(date_moche = str_detect(buildings$date_construction, "^(?!\\d{4}$).+"))%>%
  mutate(date_belle = case_when(date_moche == FALSE ~ date_construction, TRUE ~ "no date"))%>%
  mutate(style_archi = case_when(date_construction == "до 1917" | date_construction == "до 1914" ~ "imperial",
                                 date_belle <= 1917 ~ "imperial",
                                 date_belle >= 1918 & date_belle <= 1934 ~ "constructivist",
                                 date_belle >= 1935 & date_belle <= 1955 ~ "stalinist",
                                 date_belle >= 1956 & date_belle <= 1965 ~ "khrushchevian",
                                 date_belle >= 1966 & date_belle <= 1975 ~ "brezhnevist",
                                 date_belle >= 1976 & date_belle <= 1990 ~ "latesoviet",
                                 date_belle >= 1991 & date_belle <= 2020 ~ "postsoviet",
                                 TRUE ~ NA))


# Batiments qui n'ont pas pu être classés
pas_de_style <- buildings %>%
  dplyr::select(X, date_construction, date_reconstruction, style_archi) %>%
  filter(is.na(style_archi))

# enlever les variables utiles au traitement 
buildings <- buildings %>% 
  dplyr::select(-date_belle,-date_moche)

# nettoyage des variables de type d'appartement
# Pivot comm_nb selon comm type ---------------------------------------------------------

## 1) Sélectionner les variables qui nous intéressent ainsi que la clé primaire

comm_appartments <- buildings %>% 
  dplyr::select(X, comm_type, comm_nb)

## 2) Séparer en autant de colonnes les modalités des nb de pièces : 2 pièces, 3 pièces, ...

# modalites_nb_pieces_comm <- unique(unlist(unique(str_extract_all(comm_appartments$comm_type, "\\d* room")), recursive = F))

comm_appartments_reshaped <- comm_appartments %>%
  separate_rows(comm_type, comm_nb, sep = ", {0,1}") %>%
  spread(comm_type, comm_nb) %>% 
  dplyr::select(-c("<NA>"))



colnames(comm_appartments_reshaped)[-c(1)] <- paste(str_extract(colnames(comm_appartments_reshaped[-c(1)]), "\\d+"), "room_comm", sep = "_")

cols_with_room <- grep("room", names(comm_appartments_reshaped))

comm_appartments_reshaped[,cols_with_room] <- lapply(comm_appartments_reshaped[,cols_with_room], as.numeric)

#comm_appartments_reshaped <- comm_appartments_reshaped %>% 
#  select(-V1)

## 3) Pivoter suivant ces colonnes

comm_appartments_reshaped_long <- comm_appartments_reshaped %>% 
  pivot_longer(cols = c("2_room_comm", "3_room_comm", "4_room_comm", "5_room_comm", "6_room_comm", "7_room_comm"),
               names_to = "comm_appartment_type",
               values_to = "comm_nb_appartment_type")

comm_appartments_reshaped_long$comm_appartment_type <- as.factor(str_extract(comm_appartments_reshaped_long$comm_appartment_type, "\\d+"))

comm_appartments_reshaped[is.na(comm_appartments_reshaped)] <- 0
#comm_appartments_reshaped$X <- as.numeric(comm_appartments_reshaped$X)

#write.csv(comm_appartments_reshaped, "projet_stat_2a/code_commun/tables_jointure/comm_appartment_type_wide.csv")
#write.csv(comm_appartments_reshaped_long, "projet_stat_2a/code_commun/tables_jointure/comm_appartment_type_long.csv")



# Pivot comm_room_nb selon comm_type --------------------------------------


## 1) Sélectionner les variables qui nous intéressent ainsi que la clé primaire

comm_room_appartments <- buildings %>% 
  dplyr::select(X, comm_type, comm_room_nb)

## 2) Séparer en autant de colonnes les modalités des nb de pièces : 2 pièces, 3 pièces, ...

# modalites_nb_pieces_comm <- unique(unlist(unique(str_extract_all(comm_room_appartments$comm_type, "\\d* room")), recursive = F))

comm_room_appartments_reshaped <- comm_room_appartments %>%
  separate_rows(comm_type, comm_room_nb, sep = ", {0,1}") %>%
  spread(comm_type, comm_room_nb) %>% 
  dplyr::select(-c("<NA>"))



colnames(comm_room_appartments_reshaped)[-c(1)] <- paste(str_extract(colnames(comm_room_appartments_reshaped[-c(1)]), "\\d+"), "room_nb_comm", sep = "_")

cols_with_room <- grep("room", names(comm_room_appartments_reshaped))

comm_room_appartments_reshaped[,cols_with_room] <- lapply(comm_room_appartments_reshaped[,cols_with_room], as.numeric)

#comm_room_appartments_reshaped <- comm_room_appartments_reshaped %>% 
#  select(-V1)

## 3) Pivoter suivant ces colonnes

comm_room_appartments_reshaped_long <- comm_room_appartments_reshaped %>% 
  pivot_longer(cols = c("2_room_nb_comm", "3_room_nb_comm", "4_room_nb_comm", "5_room_nb_comm", "6_room_nb_comm", "7_room_nb_comm"),
               names_to = "comm_appartment_type",
               values_to = "comm_room_nb_appartment_type")

comm_room_appartments_reshaped_long$comm_appartment_type <- as.factor(str_extract(comm_room_appartments_reshaped_long$comm_appartment_type, "\\d+"))


comm_room_appartments_reshaped[is.na(comm_room_appartments_reshaped)] <- 0
#comm_room_appartments_reshaped$X <- as.numeric(comm_room_appartments_reshaped$X)

#write.csv(comm_room_appartments_reshaped, "projet_stat_2a/steve/tables_jointure/comm_room_appartment_type_wide.csv")
#write.csv(comm_room_appartments_reshaped_long, "projet_stat_2a/steve/tables_jointure/comm_room_appartment_type_long.csv")



# Pivot flat_nb selon flat type ---------------------------------------------------------

## 1) Sélectionner les variables qui nous intéressent ainsi que la clé primaire

flat_appartments <- buildings %>% 
  dplyr::select(X, flat_type, flat_nb)

## 2) Séparer en autant de colonnes les modalités des nb de pièces : 2 pièces, 3 pièces, ...

# modalites_nb_pieces_flat <- unique(unlist(unique(str_extract_all(flat_appartments$flat_type, "\\d* room")), recursive = F))

flat_appartments_reshaped <- flat_appartments %>%
  separate_rows(flat_type, flat_nb, sep = ", {0,1}") %>%
  spread(flat_type, flat_nb) %>% 
  dplyr::select(-c("<NA>"))



colnames(flat_appartments_reshaped)[-c(1)] <- paste(str_extract(colnames(flat_appartments_reshaped[-c(1)]), "\\d+"), "room_flat", sep = "_")

cols_with_room <- grep("room", names(flat_appartments_reshaped))

flat_appartments_reshaped[,cols_with_room] <- lapply(flat_appartments_reshaped[,cols_with_room], as.numeric)

#flat_appartments_reshaped <- flat_appartments_reshaped %>% 
#  select(-V1)

## 3) Pivoter suivant ces colonnes

flat_appartments_reshaped_long <- flat_appartments_reshaped %>% 
  pivot_longer(cols = c("1_room_flat", "2_room_flat", "3_room_flat", "4_room_flat", "5_room_flat", "6_room_flat", "7_room_flat"),
               names_to = "flat_appartment_type",
               values_to = "flat_nb_appartment_type")

flat_appartments_reshaped_long$flat_appartment_type <- as.factor(str_extract(flat_appartments_reshaped_long$flat_appartment_type, "\\d+"))

flat_appartments_reshaped[is.na(flat_appartments_reshaped)] <-0
#flat_appartments_reshaped$X <- as.numeric(flat_appartments_reshaped$X)

#write.csv(flat_appartments_reshaped, "projet_stat_2a/steve/tables_jointure/flat_appartment_type_wide.csv")
#write.csv(flat_appartments_reshaped_long, "projet_stat_2a/steve/tables_jointure/flat_appartment_type_long.csv")

# Pivot flat_room_nb selon flat type ---------------------------------------------------------

## 1) Sélectionner les variables qui nous intéressent ainsi que la clé primaire

flat_room_appartments <- buildings %>% 
  dplyr::select(X, flat_type, flat_room_nb)

## 2) Séparer en autant de colonnes les modalités des nb de pièces : 2 pièces, 3 pièces, ...

# modalites_nb_pieces_flat <- unique(unlist(unique(str_extract_all(flat_appartments$flat_type, "\\d* room")), recursive = F))

flat_room_appartments_reshaped <- flat_room_appartments %>%
  separate_rows(flat_type, flat_room_nb, sep = ", {0,1}") %>%
  spread(flat_type, flat_room_nb) %>% 
  dplyr::select(-c("<NA>"))



colnames(flat_room_appartments_reshaped)[-c(1)] <- paste(str_extract(colnames(flat_room_appartments_reshaped[-c(1)]), "\\d+"), "room_nb_flat", sep = "_")

cols_with_room <- grep("room", names(flat_room_appartments_reshaped))

flat_room_appartments_reshaped[,cols_with_room] <- lapply(flat_room_appartments_reshaped[,cols_with_room], as.numeric)

#flat_room_appartments_reshaped <- flat_room_appartments_reshaped %>% 
#  select(-V1)

## 3) Pivoter suivant ces colonnes

flat_room_appartments_reshaped_long <- flat_room_appartments_reshaped %>% 
  pivot_longer(cols = c("1_room_nb_flat", "2_room_nb_flat", "3_room_nb_flat", "4_room_nb_flat", "5_room_nb_flat", "6_room_nb_flat", "7_room_nb_flat"),
               names_to = "flat_room_appartment_type",
               values_to = "flat_room_nb_appartment_type")

flat_room_appartments_reshaped_long$flat_room_appartment_type <- as.factor(str_extract(flat_room_appartments_reshaped_long$flat_room_appartment_type, "\\d+"))

flat_room_appartments_reshaped[is.na(flat_room_appartments_reshaped)] <- 0
#flat_room_appartments_reshaped$X <- as.numeric(flat_room_appartments_reshaped$X)

#write.csv(flat_room_appartments_reshaped, "projet_stat_2a/steve/tables_jointure/flat_room_appartment_type_wide.csv")
#write.csv(flat_room_appartments_reshaped_long, "projet_stat_2a/steve/tables_jointure/flat_room_appartment_type_long.csv")



# jointure --------------------------------------------------------

#chemin <- "projet_stat_2a/code_commun/tables_jointure"
#fichiers_wide <- list.files(path = chemin, pattern = "_wide\\.csv$")

#for (fichier in fichiers_wide){
#  assign(str_remove_all(fichier, "\\.csv$"),
#         read_csv(paste(chemin, fichier, sep = "/"))[,-c(1)])
#}

BUILDINGS <- buildings %>% 
  inner_join(comm_appartments_reshaped) %>% 
  inner_join(comm_room_appartments_reshaped) %>% 
  inner_join(flat_appartments_reshaped) %>% 
  inner_join(flat_room_appartments_reshaped)

BUILDINGS$nb_protest <- as.numeric(BUILDINGS$nb_protest)
BUILDINGS$nb_residents <- as.numeric(BUILDINGS$nb_residents)
BUILDINGS$nb_floors <- as.numeric(BUILDINGS$nb_floors)
BUILDINGS$nb_com_flats <- as.numeric(BUILDINGS$nb_com_flats)
BUILDINGS$nb_flats <- as.numeric(BUILDINGS$nb_flats)
BUILDINGS$nb_all_flats <- as.numeric(BUILDINGS$nb_all_flats)
BUILDINGS$nb_com_rooms <- as.numeric(BUILDINGS$nb_com_rooms)
BUILDINGS$nb_rooms <- as.numeric(BUILDINGS$nb_rooms)
BUILDINGS$nb_all_rooms <- as.numeric(BUILDINGS$nb_all_rooms)

# Get the names of the columns ending in "_area"
columns_area <- names(BUILDINGS)[grepl("_area", names(BUILDINGS))]

# Change the column types to numeric
BUILDINGS[columns_area] <- lapply(BUILDINGS[columns_area], as.numeric)
BUILDINGS[,columns_area] <- lapply(BUILDINGS[,columns_area],function(x) replace(x,is.na(x),0))

BUILDINGS$waste_chute_nb <- as.numeric(BUILDINGS$waste_chute_nb)
BUILDINGS$nb_nonresidential <- as.numeric(BUILDINGS$nb_nonresidential)
BUILDINGS$lift_nb <- as.numeric(BUILDINGS$lift_nb)
BUILDINGS$intercom_nb <- as.numeric(BUILDINGS$intercom_nb)

### pré traitement de la table ###
#transformation des num 1/0 en facteur oui/non
BUILDINGS$dilapidated <-factor(x = BUILDINGS$dilapidated, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$heating_central <-factor(x = BUILDINGS$heating_central, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$heating_boiler <-factor(x = BUILDINGS$heating_boiler, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$heating_stove <-factor(x = BUILDINGS$heating_stove, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$water_central <-factor(x = BUILDINGS$water_central, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$water_gas <-factor(x = BUILDINGS$water_gas, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$water_wood <-factor(x = BUILDINGS$water_wood, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$electr_central <-factor(x = BUILDINGS$electr_central, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$gas_central <-factor(x = BUILDINGS$gas_central, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$gas_noncentral <-factor(x = BUILDINGS$gas_noncentral, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$waste_chute <-factor(x = BUILDINGS$waste_chute, levels=c("oui", "non"), labels=c("oui", "non"))
BUILDINGS$waste_chute_nb[is.na(BUILDINGS$waste_chute_nb)] <- 0
BUILDINGS$nb_nonresidential[is.na(BUILDINGS$nb_nonresidential)] <- 0
BUILDINGS$lift_nb[is.na(BUILDINGS$lift_nb)] <- 0
BUILDINGS$intercom_nb[is.na(BUILDINGS$intercom_nb)] <- 0

# Liste dates de construction pas belles (code de Steve)
library(stringr)
liste_dates_pas_bien <- unique(buildings$date_construction[str_detect(buildings$date_construction, "^(?!\\d{4}$).+")])
str(liste_dates_pas_bien, nchar.max = 125)

# Classification styles
library(dplyr)
BUILDINGS <- BUILDINGS %>%
  mutate(date_moche = str_detect(buildings$date_construction, "^(?!\\d{4}$).+"))%>%
  mutate(date_belle = case_when(date_moche == FALSE ~ date_construction, TRUE ~ "no date"))%>%
  mutate(style_archi = case_when(date_construction == "до 1917" | date_construction == "до 1914" ~ "imperial",
                                 date_belle <= 1917 ~ "imperial",
                                 date_belle >= 1918 & date_belle <= 1934 ~ "constructivist",
                                 date_belle >= 1935 & date_belle <= 1955 ~ "stalinist",
                                 date_belle >= 1956 & date_belle <= 1965 ~ "khrushchevian",
                                 date_belle >= 1966 & date_belle <= 1975 ~ "brezhnevist",
                                 date_belle >= 1976 & date_belle <= 1990 ~ "latesoviet",
                                 date_belle >= 1991 & date_belle <= 2020 ~ "postsoviet",
                                 date_construction == "1960- 1975" | date_construction == "1971, 1973" ~ "brezhnevist",
                                 date_construction == "1855-1893" | date_construction == "1843-1914" | date_construction == "1823-1899" | date_construction == "1878-1908" | date_construction == "1798-1896" | date_construction == "1810-1822" | date_construction == "1874-1911" | date_construction == "1728, 1813, 1828" ~ "imperial", 
                                 date_construction == "1875-1877" | date_construction == "1857-1860" | date_construction == "1836-1841" | date_construction == "1879-1884" | date_construction == "1855-1907" | date_construction == "1756;1870-1898" | date_construction == "1756-1849" | date_construction == "1798-1900" ~ "imperial",
                                 date_construction == "1828, 1846" | date_construction == "1846-1899" | date_construction == "1832, 1912" | date_construction == "1876-1891" |date_construction == "1826-1873" | date_construction == "1842-1861" | date_construction == "1828,1851,1874" | date_construction == "1901-1913" ~ "imperial",
                                 date_construction == "1957-58" | date_construction == "1958-1959" | date_construction == "1961-1963" | date_construction == "1828-1961" | date_construction == "1957, 1958" | date_construction == "1956, 1963" | date_construction == "1957-1958" ~ "khrushchevian", 
                                 date_construction == "1976,1978" | date_construction == "1976, 1981" | date_construction == "1856-1986" | date_construction == "1978,1979" | date_construction == "1980,1981" | date_construction == "1979-1983, 1997" | date_construction == "1857, 1902, 1990" ~ "latesoviet",
                                 date_construction == "1974-1975" | date_construction == "1973-1974" | date_construction == "до 1971" ~ "brezhnevist",
                                 date_construction == "1898-1900" | date_construction == "1859-1882" | date_construction == "1798-1902" | date_construction == "1879-1908" | date_construction == "до 1919" | date_construction == "1859-1898" | date_construction == "1855-1872" | date_construction == "1826-1866" ~ "imperial",
                                 date_construction == "1956,1964" ~ "khrushchevian",
                                 date_construction == "1900, 1910" | date_construction == "1798-1884" | date_construction == "1823, 1849" | date_construction == "1857-1873" | date_construction == "1828-1900" | date_construction == "1798-1859" | date_construction == "1858-1911" | date_construction == "1839-1874" ~ "imperial",
                                 date_construction == "2009, 2013" | date_construction == "1995-1996" | date_construction == "2010-2012" | date_construction == "200" | date_construction == "2000,2002,2003" | date_construction == "1997-2001" | date_construction == "2001, 2005" | date_construction == "2000,2001" ~ "postsoviet",
                                 date_construction == "1933, 1950-литер Г" | date_construction == "1879/1952" | date_construction == "1952,1954" | date_construction == "1917, 1936" ~ "stalinist",
                                 date_construction == "1904-1926" | date_construction == "1877-1928" ~ "constructivist", 
                                 date_construction == "1842-1880" | date_construction == "1837-1858" | date_construction == "1847-1860" | date_construction == "1822-1876" | date_construction == "1853-1904" | date_construction == "1846-1905" | date_construction == "1828-1866" | date_construction == "1837-1902" ~ "imperial",
                                 date_construction == "1828,1904" | date_construction == "1883-1906" | date_construction == "1857-1865" | date_construction == "1858-1887" | date_construction == "1832,1896" | date_construction == "1911,1887,1884" | date_construction == "1897, 1904" | date_construction == "1856-1889" ~ "imperial",
                                 date_construction == "1881-1836" | date_construction == "1798-1858" | date_construction == "1838-1890" | date_construction == "1857-1870" | date_construction == "1870-1908" | date_construction == "1838-1900" | date_construction == "1884-1902" | date_construction == "1906-1909" ~ "imperial",
                                 date_construction == "2005-2006" | date_construction == "2010,2011" | date_construction == "2000-2001" | date_construction == "1993, 2005" | date_construction == "2003,2004"| date_construction == "1991-1994" | date_construction == "2001, 2002" | date_construction == "2005,2007" ~ "postsoviet",
                                 date_construction == "1978,1987" | date_construction == "1985-1987" | date_construction == "1985-1986" | date_construction == "1976, 1982" ~ "latesoviet",
                                 date_construction == "1813-1891" | date_construction == "1813-1875" | date_construction == "1840-1880" | date_construction == "1840-1880" | date_construction == "1860-1874" | date_construction == "1886-1907" | date_construction == "1886-1907" | date_construction == "1860-1869" ~ "imperial",
                                 date_construction == "1828-1852" | date_construction == "1905-1907" | date_construction == "1861-1902" ~ "imperial",
                                 date_construction == "2010, 2012" ~ "postsoviet",
                                 TRUE ~ NA))


# Batiments qui n'ont pas pu être classés
pas_de_style <- BUILDINGS %>%
  dplyr::select(X, date_construction, date_reconstruction, style_archi) %>%
  filter(is.na(style_archi))

# enlever les variables utiles au traitement 
BUILDINGS <- BUILDINGS %>% 
  dplyr::select(-date_belle,-date_moche)


# jointure RZT_to_demolish à buildings
library(readr)
library(dplyr)
to_demolish_geocoded <- read_csv("projet_stat_2a/Sources/to_demolish_geocoded.csv") %>%
  dplyr::select(-...1, -id) %>% 
  mutate(addr_number = as.character(addr_number),
         addr_building = as.character(addr_building),
         lon = as.character(round(lon, digits = 5)),
         lat = as.character(round(lat, digits = 5)),
         building_to_demolish = 1)

test <- left_join(BUILDINGS, to_demolish_geocoded, by = join_by(district, RZT, addr_street, addr_number, addr_building, addr_letter, lon, lat))
test2 <- left_join(BUILDINGS, to_demolish_geocoded, by = join_by(lon, lat, addr_number, addr_building, addr_letter))
unique(test$building_to_demolish)
which(test$building_to_demolish == 1)
which(test2$building_to_demolish == 1)

# ajout de nvl var !
BUILDINGS <- BUILDINGS %>% 
  mutate(mean_room_nb_in_com_flats = ifelse(nb_com_flats == 0, NA, nb_com_rooms/nb_com_flats), # Nombre moyen de pièces d'un appartement communautaire au sein du bâtiment
         mean_room_nb_in_flats = ifelse(nb_flats == 0, NA, nb_rooms/nb_flats), # Nombre moyen de pièces d'un appartement non communautaire au sein du bâtiment
         mean_room_nb_in_all_flats = ifelse(nb_all_flats == 0, NA, nb_all_rooms/nb_all_flats), # Nombre moyen de pièces d'un appartement (communautaire et non comm confondus)
         living_area_for_one_resident = ifelse(nb_residents == 0, NA, living_area/nb_residents) # Surface moyenne pour un habitant au sein d'un bâtiment
  )

types_protest <- read_csv("projet_stat_2a/Sources/types_protest.csv") %>% 
  rename(RZT = neighbourhood)

BUILDINGS <- left_join(BUILDINGS, types_protest, by = 'RZT') %>% 
  dplyr::select(-nb_protest.x) %>% 
  rename(nb_protest = nb_protest.y)

col_names_type_prot <- names(types_protest)[3:length(names(types_protest))]
BUILDINGS[,col_names_type_prot] <- lapply(BUILDINGS[,col_names_type_prot],function(x) replace(x,is.na(x),0))

# RZT Narvskaya Zastava de Kolpinsky vers Kirovsky
BUILDINGS <- BUILDINGS %>%
  mutate(district = case_when(RZT == "Narvskaya Zastava" ~ "Kirovsky",
                              TRUE ~ district))

#Création d'un facteur de heating
BUILDINGS <- BUILDINGS %>%
  mutate(heating = case_when(heating_central == "oui" ~ "central",
                                  heating_boiler == "oui" ~ "boiler",
                                  heating_stove == "oui" ~ "stove",
                                  TRUE ~ NA))
BUILDINGS <- BUILDINGS %>%
  dplyr::select(-heating_central, -heating_boiler, -heating_stove)

#Création d'un facteur de water
BUILDINGS <- BUILDINGS %>%
  mutate(water_heating = case_when(water_central == "oui" ~ "central",
                             water_gas == "oui" ~ "gas",
                             water_wood == "oui" ~ "wood",
                             TRUE ~ "no water heating"))
BUILDINGS <- BUILDINGS %>%
  dplyr::select(-water_central, -water_gas, -water_wood)

#Création d'un facteur de gas
BUILDINGS <- BUILDINGS %>%
  mutate(gas = case_when(gas_central == "oui" ~ "central",
                         gas_noncentral == "oui" ~ "noncentral",
                         TRUE ~ "no gas"))

BUILDINGS <- BUILDINGS %>%
  dplyr::select(-gas_central, -gas_noncentral)

write.csv(BUILDINGS, "projet_stat_2a/code_commun/tables/BUILDINGS.csv")

# Table des bâtiments au sein d'un quartier de rénovation
BUILDINGS_RZT <- BUILDINGS %>% 
  filter(!is.na(RZT)) %>%
  group_by(RZT)%>%
  mutate(nb_bat_RZT = sum(!is.na(RZT))) %>%
  mutate(poids_protest = nb_protest/nb_bat_RZT) %>% 
  ungroup()

write.csv(BUILDINGS_RZT, "~/work/projet_stat_2a/code_commun/tables/BUILDINGS_RZT.csv")
