buildings <- read.csv("projet_stat_2a/Sources/buildings.csv")
View(buildings)

# Dates repair ??? quésako y'en a beaucoup

library(stringr)

# Nombre de dates de constructions avec plusieurs
sum(str_detect(buildings$date_construction, "(\\d{4})+.*(\\d{4})+"), na.rm = T)

# Nombre de bâtiments dont date construction est inconnue
sum(str_detect(buildings$date_construction, "неизвестно"), na.rm = T)

# en priorité STEVE
# pivoter comm nb, comm room nb par comm type
# pivoter flat_nb, flat_room_nb par flat_type

# pivoter les obs suivant la foultitude de dates constructions/reconstructions

# PAS MOI
# changer le "avant 2017" en "2017" dans une nouvelle colonne
# travail de NA

# Date de premier répa, date de dernière répa
# Nb de répa


unique(buildings$management)

sum(buildings$management == "public", na.rm = T)
sum(buildings$management == "", na.rm = T)

library(sp)
library(sf)
library(dplyr)
library(ggplot2)
library(gstat)
library(ggspatial)

buildings_sp <- buildings %>% 
  filter(!is.na(lon)) %>% 
  filter(!is.na(lat)) %>% 
  select(X, nb_protest, lon, lat)

#Create Spatial Points object
buildings_sp <- st_as_sf(buildings_sp, coords = c("lon", "lat"), crs = 5933)

#Add geometry column to data frame
buildings_sp$geometry <- st_geometry(buildings_sp)
buildings_sp <- st_transform(x = buildings_sp, crs = 4200)

library(sf)
library(dplyr)
library(crsuggest)
library(ggrepel)
russia_map <- st_read("projet_stat_2a/fonds_carte/SPB_admin_boundary.shp")

# CRS DE BASE : 4326 (WGS 84)
guess_crs(russia_map, c(29.76864,
                        59.90482))
russia_map <- st_set_crs(russia_map, 5933)
russia_map <- st_transform(russia_map, 4200)

guess_crs(buildings_sp, c(30.2637, 60.01434))

russia_map_arrond <- russia_map %>%
  filter(ADMIN_LVL == "5") %>% 
  mutate(NAME = str_remove_all(NAME, " район"))

russia_map_entity <- russia_map %>% 
  filter(ADMIN_LVL == "8")

plot(russia_map %>%
  filter(ADMIN_LVL == "8") %>% 
    select(NAME))

unique(russia_map$ADMIN_LVL)

buildings_protests <- total_bat_district %>% 
  left_join(russia_map_arrond, by = c("addr_district" = "NAME"))

buildings_protests_aggr <- buildings_protests %>% 
  group_by(addr_district, district, geometry) %>% 
  summarise(nb_protest = sum(nb_protest, na.rm = T)) %>% 
  mutate(geometry = st_transform(geometry, crs = 4326))

guess_crs(buildings_protests_aggr, c(33.00323, 65.10169))

protest_arrond_tmp <- buildings_sp %>%
  rename(g_p = geometry) %>%
  filter(!is.na(nb_protest))




protest_arrond <- st_join(protest_arrond_tmp, russia_map_arrond, join = st_nearest_feature, left = T)
unique(protest_arrond$fid)


theme_set(theme_void())
theme_update(text = element_text(family = "Marianne"), 
             plot.background = element_rect(fill = "white", color = NA),
             plot.margin = margin(25, 25, 10, 25),
             plot.title.position = "plot",
             plot.caption = element_text(size = 6),
             plot.caption.position = "plot")

br = quantile(buildings_protests_aggr$nb_protest[!is.na(buildings_protests_aggr$nb_protest)], probs = seq(from = 0, to = 1, by = 0.2))
names(br) <- NULL

br_modif = c(0, 10, 20, 30, 50)

districts <- buildings_protests_aggr
districts$geometry <- st_centroid(districts$geometry)

ggplot(buildings_protests_aggr, aes(geometry = geometry)) +
  geom_sf() +
  #geom_sf(data = buildings_sp, fill = NA, color = gray(.5)) +
  #layer_spatial(iris, size = .2, fill = NA, color = "grey") +
  layer_spatial(buildings_protests_aggr, aes(fill = nb_protest)) +
  scale_fill_fermenter(name = "Nombre de\ncontestations", 
                       palette = "RdPu", 
                       na.value = "gray",
                       breaks = br_modif, # à ajuster selon besoin
                       direction = 1) +
  #coord_sf(xlim = c(st_bbox(russia_map_arrond)[[1]], st_bbox(russia_map_arrond)[[3]]), 
  #         ylim = c(st_bbox(russia_map_arrond)[[2]], st_bbox(russia_map_arrond)[[4]]),
  #         crs = 4200) +
  layer_spatial(districts, size = 1, color = "black") +
  geom_text_repel(data = districts,  
                  aes(geometry = geometry, label = str_wrap(district, 12)),
                  stat = "sf_coordinates",
                  size = 5,
                  bg.color = "#ffffff66",
                  bg.r = 0.2) +
  annotation_scale(location = "bl", 
                   height = unit(.15, "cm"), 
                   line_col ="grey",
                   text_col = "grey",
                   bar_cols = c("grey", "white")) +
  labs(title = "Nombre de contestations à Saint-Pétersbourg",
       subtitle = "à l'échelle du district\n",
       caption = "__")



ibrary(tidyverse)
library(janitor)
library(glue)
library(sf)
library(fs)
library(rlang)
library(ggspatial)
library(ggrepel)

source("projet_stat_2a/steve/lissage/fonctions_lissage.R", encoding = "UTF-8")

rayon <- 10000
pixel <- 1000
epsg <- 4200
reg <- russia_map %>% 
  filter(ADMIN_LVL == "8")

lissage(buildings_sp[!is.na(buildings_sp$nb_protest),], nb_protest, rayon, pixel, NULL, epsg) %>%
  raster::writeRaster(glue("projet_stat_2a/steve/resultat_carto/nb_protest.tif"),
                      options = c("COMPRESS=DEFLATE",
                                  "ZLEVEL=9",
                                  # "NUM_THREADS=ALL_CPUS",
                                  "PREDICTOR=3"),
                      overwrite = TRUE)

lissage_raster <- "projet_stat_2a/steve/resultat_carto/nb_protest.tif" %>% 
  glue() %>% 
  raster::raster() 

"projet_stat_2a/steve/resultat_carto/nb_protest.tif" %>% 
  glue() %>% 
  raster::raster() %>% 
  raster::plot()

# carte
ggplot() +
  layer_spatial(lissage_raster, aes(fill = stat(1000^2 * band1 / pixel^2))) +
  #layer_spatial(pref, size = 1, color = "black") +
  scale_fill_fermenter(name = "densité\nvaches laitières\n(têtes/km²)", 
                       palette = "YlGnBu", 
                       na.value = "white",
                       breaks = br, # à ajuster selon besoin
                       direction = 1) +
  annotation_scale(location = "bl", 
                   height = unit(.15, "cm"), 
                   line_col ="grey",
                   text_col = "grey",
                   bar_cols = c("grey", "white")) +
  labs(title = "Bovins lait",
       subtitle = glue("ss"),
       caption = glue("tt"))


pas <- 100 #200-meter square
rayon <- 1000 #400-meter bandwith
#smoothing function call
#the function automatically smoothes all the variables contained in the database
#here, phouhold and houhold are smoothed
nb_protest_smooth <- btb::kernelSmoothing(buildings_sp[!is.na(buildings_sp$nb_protest),], iCellSize = pas,
                                iBandwidth = rayon, sEPSG="4200")


#overview in R
library(sp)
library(cartography)
#map display
cartography::choroLayer(nb_protest_smooth, var = "nb_protest", nclass = 5, border = NA, legend.pos = "topright", legend.title.txt =
                          "nb_protest")
#title and outline added
cartography::layoutLayer(title = "Reunion Island : rate of poor households"
                         ,
                         sources = "",
                         author = "",
                         scale = NULL,
                         frame = TRUE,
                         col = "black",
                         coltitle = "white")








russia_map_arrond$NAME <- str_remove_all(russia_map_arrond$NAME, " район")
all(russia_map_arrond$NAME %in% unique(buildings$addr_district))
all(unique(buildings$addr_district) %in% russia_map_arrond$NAME)



buildings_protests <- buildings %>% 
  select(X, addr_district, nb_protest) %>% 
  left_join(russia_map_arrond, by = c("addr_district" = "NAME"))

